package dipos.ris.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.repo.CargoRepo;

@Service
@Transactional
public class CargoServiceImpl implements CargoService{

	@Autowired
	private CargoRepo cargoRepo;
	
	@Override
	public List<Cargo> listarCargos() {
		return (List<Cargo>) cargoRepo.findAll();
	}

	@Override
	public Cargo obtenerCargo(int id) {
		return cargoRepo.findById(id).get();
	}

	@Override
	public Cargo grabarCargo(Cargo car) {
		return cargoRepo.save(car);
	}

	@Override
	public boolean cargoExiste(int id) {		
		return cargoRepo.existsById(id);
	}

}
