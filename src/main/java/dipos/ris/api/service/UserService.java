package dipos.ris.api.service;

import dipos.ris.api.domain.Role;
import dipos.ris.api.domain.User;

import java.util.List;

public interface UserService {
    User saveUser(User user);
    Role saveRole(Role role);
    void addRoleToUser(String username, String roleName);
    User getUser(String username);
    List<User> getUsers();
}
