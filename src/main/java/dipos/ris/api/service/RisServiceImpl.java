package dipos.ris.api.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.Ris;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class RisServiceImpl implements RisService
{
    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
	@Override
    public List<Ris> getListRis() {
        String query = "FROM Ris group by id_ris, ris";
        List<Ris> lista = entityManager.createQuery(query)
                .getResultList();
        if(lista.isEmpty()) {
            return null;
        }
        return lista;
    }

	@SuppressWarnings("unchecked")
	@Override
	public Ris getListRisObtenerOne(int idRis) {
		Ris risOne = null;
		 String query = "FROM Ris r where r.id_ris = :idRis";
		 
		 Query querySQL = entityManager.createQuery(query);
		 querySQL.setParameter("idRis", idRis);
	        List<Ris> listado =  querySQL.getResultList();
	       if(listado.size()>0)
	    	   risOne = listado.get(0);
	        return risOne;
	}
	
}