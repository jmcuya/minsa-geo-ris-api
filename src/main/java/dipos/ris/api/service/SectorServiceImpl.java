package dipos.ris.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.Sector;
import dipos.ris.api.repo.CargoRepo;
import dipos.ris.api.repo.SectorRepo;

@Service
@Transactional
public class SectorServiceImpl implements SectorService{

	@Autowired
	private SectorRepo sectorRepo;
	
	@Override
	public List<Sector> listarSector() {
		return (List<Sector>) sectorRepo.findAll();
	}

}
