package dipos.ris.api.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.NominalizationLegacy;
import dipos.ris.api.domain.NominalizationLegacyResponse;
import dipos.ris.api.domain.TablaComodinRis;
import dipos.ris.api.repo.TablaComodinRisRepository;
import dipos.ris.api.request.NominalizationRequest;

@Service
@Transactional
public class NominalizationServiceImpl implements NominalizationService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private TablaComodinRisRepository tablaComodinRisRepository;
	
	@Value("${sistema.prefijotabla}")
	private String prefijoTabla;

	@Override
	public List<NominalizationLegacyResponse> getNominalizationList(NominalizationRequest req, String type) {
		List<NominalizationLegacyResponse> listadoResponse = new ArrayList<NominalizationLegacyResponse>();
//		List<TablaComodinRis> listaTablaComodinRis = tablaComodinRisRepository
//				.getNominalizationBuscador("nominalizacionlegacy", "00", "", "", "", "", "", "", 0, 0 ,0);
//		
		List<TablaComodinRis> listaTablaComodinRis = tablaComodinRisRepository
				.getNominalizationBuscador(prefijoTabla+req.getRis().getValue(), 
											req.getDni(), 
											req.getNombres(), 
											req.getPaterno(), 
											req.getMaterno(), 
											req.getGenero(), 
											req.getEdadIni(), 
											req.getEdadFin(), 
											req.getIdZona(), 
											req.getIdIafas(),
											req.getIdSector());

		listaTablaComodinRis.forEach(t -> {
			NominalizationLegacyResponse res = new NominalizationLegacyResponse();
			BeanUtils.copyProperties(t, res);
			listadoResponse.add(res);
		});

		System.out.println(listaTablaComodinRis);

		return listadoResponse;
	}

	@Override
	public String generarReporte(List<TablaComodinRis> listado) {
		// TODO Auto-generated method stub
		return null;
	}

}
