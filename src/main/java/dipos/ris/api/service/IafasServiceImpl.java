package dipos.ris.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.Iafas;
import dipos.ris.api.repo.CargoRepo;
import dipos.ris.api.repo.IafasRepo;

@Service
@Transactional
public class IafasServiceImpl implements IafasService{

	@Autowired
	private IafasRepo iafasRepo;
	
	@Override
	public List<Iafas> listarIafas() {
		return (List<Iafas>) iafasRepo.findAll();
	}

}
