package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.CondLaboral;

public interface CondLaboralService {

	public List<CondLaboral> listarCondLaborales();
	public CondLaboral obtenerCondLaboral(int id);
	public CondLaboral grabarCondLaboral(CondLaboral conlab);
	public boolean condLaboralExiste(int id);
	
}
