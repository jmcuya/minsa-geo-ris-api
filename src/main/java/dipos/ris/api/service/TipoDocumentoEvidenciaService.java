package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.TipoDocEvidencia;

public interface TipoDocumentoEvidenciaService {

	public List<TipoDocEvidencia> listarTipoDocumentos();
	public TipoDocEvidencia obtenerTipoDocumento(int id);
	public TipoDocEvidencia grabarTipoDocumento(TipoDocEvidencia tipdoc);
	public boolean tipoDocumentoExiste(int id);
	
}
