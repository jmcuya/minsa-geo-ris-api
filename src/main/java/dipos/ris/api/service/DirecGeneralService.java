package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.DirecGeneral;

public interface DirecGeneralService {

	public List<DirecGeneral> listarDirecGenerales();
	public DirecGeneral obtenerDirecGeneral(int id);
	public DirecGeneral grabarDirecGeneral(DirecGeneral dirgen);
	public boolean direcGeneralExiste(int id);
	
}
