package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.Tarea;

public interface TareaService {

	public List<Tarea> listarTareas();

	public Tarea grabarTarea(Tarea tar);

	public boolean tareaExiste(int id);
	
}
