package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.NominalizationLegacyResponse;
import dipos.ris.api.domain.TablaComodinRis;
import dipos.ris.api.request.NominalizationRequest;

public interface NominalizationService {
    List<NominalizationLegacyResponse> getNominalizationList(NominalizationRequest nominalization, String type);
	
	String generarReporte(List<TablaComodinRis> listado);
}
