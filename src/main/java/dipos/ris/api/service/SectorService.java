package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.Sector;

public interface SectorService {

	public List<Sector> listarSector();
	
}
