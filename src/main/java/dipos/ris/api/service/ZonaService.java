package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.Zona;

public interface ZonaService {

	public List<Zona> listarZona();
	
}
