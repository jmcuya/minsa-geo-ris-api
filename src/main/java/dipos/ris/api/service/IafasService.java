package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.Iafas;

public interface IafasService {

	public List<Iafas> listarIafas();
	
}
