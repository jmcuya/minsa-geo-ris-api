package dipos.ris.api.service;


import java.util.List;

import org.springframework.data.domain.Pageable;

import dipos.ris.api.request.UsuarioRequest;
import dipos.ris.api.response.DevolverGrillaUsuariosDto;
import dipos.ris.api.response.UsuarioResponse;

public interface UsuarioService {

	public List<UsuarioResponse> listarUsuarios();
	public UsuarioResponse obtenerUsuario(int id);
	public UsuarioResponse grabarUsuario(UsuarioRequest usu);
	public boolean usuarioExiste(int id);
	public DevolverGrillaUsuariosDto listarGrillaUsuarios(UsuarioRequest req, Pageable pageable);
}
