package dipos.ris.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.Zona;
import dipos.ris.api.repo.CargoRepo;
import dipos.ris.api.repo.ZonaRepo;

@Service
@Transactional
public class ZonaServiceImpl implements ZonaService{

	@Autowired
	private ZonaRepo zonaRepo;
	
	@Override
	public List<Zona> listarZona() {
		return (List<Zona>) zonaRepo.findAll();
	}

}
