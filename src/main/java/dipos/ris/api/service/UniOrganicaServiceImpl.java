package dipos.ris.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.UniOrganica;
import dipos.ris.api.repo.UniOrganicaRepo;

@Service
@Transactional
public class UniOrganicaServiceImpl implements UniOrganicaService{

	@Autowired
	private UniOrganicaRepo uniOrganicaRepo;
	

	@Override
	public UniOrganica obtenerUniOrganica(int id) {
		return uniOrganicaRepo.findById(id).get();
	}

	@Override
	public UniOrganica grabarUniOrganica(UniOrganica uor) {
		return uniOrganicaRepo.save(uor);
	}

	@Override
	public boolean uniOrganicaExiste(int id) {		
		return uniOrganicaRepo.existsById(id);
	}

	@Override
	public List<UniOrganica> listarUniOrganicasByDirecGeneral(int id) {
		List<UniOrganica> listadoUniOrganicas = new ArrayList<UniOrganica>();
		listadoUniOrganicas = uniOrganicaRepo.listarPorDirecGeneral(id);
		return listadoUniOrganicas;
	}

	@Override
	public List<UniOrganica> listarUniOrganicas() {
		List<UniOrganica> listadoUniOrganicas = new ArrayList<UniOrganica>();
		listadoUniOrganicas = uniOrganicaRepo.listarUniOrganicas();
		return listadoUniOrganicas;
	}

}
