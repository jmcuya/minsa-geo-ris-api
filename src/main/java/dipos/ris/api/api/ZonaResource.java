package dipos.ris.api.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.Mensaje;
import dipos.ris.api.domain.Zona;
import dipos.ris.api.service.CargoService;
import dipos.ris.api.service.ZonaService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/zona")
@RequiredArgsConstructor
public class ZonaResource {
    private final ZonaService zonaService;

    @GetMapping("/listarZona")
    @ApiOperation(value = "Este metodo lista todas las zonas")
    public ResponseEntity<List<Zona>>getCargos() {
        return ResponseEntity.ok().body(zonaService.listarZona());
    }

   
}

