package dipos.ris.api.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.Mensaje;
import dipos.ris.api.domain.Sector;
import dipos.ris.api.service.CargoService;
import dipos.ris.api.service.SectorService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/sector")
@RequiredArgsConstructor
public class SectorResource {
    private final SectorService sectorService;

    @GetMapping("/listarSector")
    @ApiOperation(value = "Este metodo lista todos los sectores")
    public ResponseEntity<List<Sector>>getSector() {
        return ResponseEntity.ok().body(sectorService.listarSector());
    }

   
}

