package dipos.ris.api.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import dipos.ris.api.domain.CondLaboral;
import dipos.ris.api.domain.Mensaje;
import dipos.ris.api.service.CondLaboralService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/condLaboral")
@RequiredArgsConstructor
public class CondLaboralResource {
    private final CondLaboralService condLaboralService;

    @GetMapping("/listarCondLaboral")
    @ApiOperation(value = "Este metodo lista todas las condiciones laborales")
    public ResponseEntity<List<CondLaboral>>getCondLaborales() {
        return ResponseEntity.ok().body(condLaboralService.listarCondLaborales());
    }

    @PostMapping("/grabarCondLaboral")
    @ApiOperation(value = "Este metodo Graba una Condición Laboral")
    public ResponseEntity<CondLaboral>grabarCondLaboral(@RequestBody CondLaboral conlab) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/condLaboral/grabarCondLaboral").toUriString());
        return ResponseEntity.created(uri).body(condLaboralService.grabarCondLaboral(conlab));
    }
    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ApiOperation(value = "Este metodo obtiene una condición laboral")
    @GetMapping(path = "/obtenerCondLaboralId/{id}")
	public ResponseEntity<CondLaboral> obtenerCondLaboral(@PathVariable(name = "id") int id){
		if(!condLaboralService.condLaboralExiste(id)) 
			return new ResponseEntity(new Mensaje("No existe la condición laboral"),HttpStatus.NOT_FOUND);
		CondLaboral conlab = condLaboralService.obtenerCondLaboral(id);
		
		return new ResponseEntity<CondLaboral>(conlab,HttpStatus.OK);
	}
	
	
	@PutMapping(path = "/actualizarCondLaboral/{id}")
	public ResponseEntity<CondLaboral> actualizarCondLaboral(@PathVariable(name = "id") int id, @RequestBody CondLaboral conlab){
		if(!condLaboralService.condLaboralExiste(id))
			return new ResponseEntity<CondLaboral>(HttpStatus.NOT_FOUND);
		
		CondLaboral conLaboralActualizado = condLaboralService.obtenerCondLaboral(id);
		BeanUtils.copyProperties(conlab, conLaboralActualizado);
		return new ResponseEntity<CondLaboral>(conlab,HttpStatus.OK);
		
	}
	
   
}

