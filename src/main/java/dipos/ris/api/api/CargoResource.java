package dipos.ris.api.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.Mensaje;
import dipos.ris.api.service.CargoService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/cargo")
@RequiredArgsConstructor
public class CargoResource {
    private final CargoService cargoService;

    @GetMapping("/listarCargo")
    @ApiOperation(value = "Este metodo lista todos los cargos")
    public ResponseEntity<List<Cargo>>getCargos() {
        return ResponseEntity.ok().body(cargoService.listarCargos());
    }

    @PostMapping("/grabarCargo")
    @ApiOperation(value = "Este metodo Graba un cargo")
    public ResponseEntity<Cargo>grabarCargo(@RequestBody Cargo car) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/cargo/grabarCargo").toUriString());
        return ResponseEntity.created(uri).body(cargoService.grabarCargo(car));
    }

    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ApiOperation(value = "Este metodo obtiene un cargo")
    @GetMapping(path = "/obtenerCargoId/{id}")
	public ResponseEntity<Cargo> obtenerCargo(@PathVariable(name = "id") int id){
		if(!cargoService.cargoExiste(id)) 
			return new ResponseEntity(new Mensaje("No existe el cargo"),HttpStatus.NOT_FOUND);
		Cargo car = cargoService.obtenerCargo(id);
		
		return new ResponseEntity<Cargo>(car,HttpStatus.OK);
	}
	
	
	@PutMapping(path = "/actualizarCargo/{id}")
	public ResponseEntity<Cargo> actualizarCargo(@PathVariable(name = "id") int id, @RequestBody Cargo car){
		if(!cargoService.cargoExiste(id))
			return new ResponseEntity<Cargo>(HttpStatus.NOT_FOUND);
		
		Cargo cargoActualizado = cargoService.obtenerCargo(id);
		BeanUtils.copyProperties(car, cargoActualizado);
		return new ResponseEntity<Cargo>(car,HttpStatus.OK);
		
	}
	
   
}

