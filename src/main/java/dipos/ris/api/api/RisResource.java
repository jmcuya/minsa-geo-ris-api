package dipos.ris.api.api;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.io.Files;

import dipos.ris.api.domain.NominalizationLegacyResponse;
import dipos.ris.api.domain.Ris;
import dipos.ris.api.request.NominalizationRequest;
import dipos.ris.api.request.PdfRequest;
import dipos.ris.api.response.PdfResponse;
import dipos.ris.api.service.NominalizationService;
import dipos.ris.api.service.RisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j
@Api("APIS de RIS")
public class RisResource {
    private final RisService risService;
    private final NominalizationService nominalizationService;

    @Value("${sistema.ruta.archivos}")
	private String rutaArchivos;
    
    @GetMapping("/ris")
    @ApiOperation(value = "Servicio para obtener ris", notes = "Retorna JSON de listado de ris.")
	public ResponseEntity<List<Ris>> getRis () {
        return ResponseEntity.ok().body(risService.getListRis());
    }

    @PostMapping("/nominalizacionList")
    public ResponseEntity<List<NominalizationLegacyResponse>> getNominalizationList (@RequestBody NominalizationRequest nominalization, @RequestParam String type) {
        return ResponseEntity.ok().body(nominalizationService.getNominalizationList(nominalization, type));
    }
    
    @CrossOrigin
	@Transactional
	@RequestMapping(value="generarReporte", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PdfResponse> generarReporte(HttpServletRequest req, @RequestBody PdfRequest pdfRequest){	
    	PdfResponse response = new PdfResponse();
		try{
			LocalDateTime ahora = LocalDateTime.now();
			
			String filePathPDFURL ="C:/Users/miSCha/Documents/miSCha/Proyectos_GIT/target/classes/static/pdf";// 
			//String filePathPDFURL = req.getSession().getServletContext().getRealPath("/Proyectos_GIT/target/classes/static/pdf");
			
			String filePathPDF=rutaArchivos;
			String filePathname = "/nombre"+RandomStringUtils.randomAlphanumeric(4)+"_A"+ahora.getYear()+"M"+ahora.getMonthValue()+"D"+ahora.getDayOfMonth()+"H"+ahora.getHour()+"m"+ahora.getHour() +".pdf";
			String mFileFullName=filePathPDF+filePathname;
			Ris rsiOne = risService.getListRisObtenerOne(pdfRequest.getCoRis());
			
			Resource resourceJasper = new ClassPathResource("static/jasper/nominalizacion_Landscape.jrxml");
			
			
			
			String rutaJasper=resourceJasper.getFile().getAbsolutePath();
	     
			Map<String, Object> parametro = new HashMap<String, Object>();
			
			parametro.put("codRis",String.valueOf(pdfRequest.getCoRis()));
			parametro.put("nomRis",rsiOne.getRis());
			
			
			
			JasperReport report=null;
			JasperPrint jasperPrint=null;
			
			List<String> mListadoPDFsGenerados = new ArrayList<String>();
			mListadoPDFsGenerados.clear();
			
			
				// cargamos el objeto al JASPER
				JRBeanCollectionDataSource ds=new JRBeanCollectionDataSource(pdfRequest.getListaTabla());

				log.info("iniciando compilacion");
				// compilamos el jasper
				report=JasperCompileManager.compileReport(rutaJasper);
				log.info("seteando variables al jasper report");
				
				log.info("rutaJasper: " + rutaJasper);
				log.info("report: " + report.getName());
				
				log.info("parametros: " + parametro.toString());
				
				jasperPrint=JasperFillManager.fillReport(report, parametro, ds);
		
				// Export to PDF.
				log.info("Exportando");
				JasperExportManager.exportReportToPdfFile(jasperPrint, mFileFullName);
				
				byte[] file = null;
				File miPDF = new File(mFileFullName);
			    file = Files.toByteArray(miPDF);
		    	Files.write(file, new File(filePathPDFURL+filePathname));
		    	
		    	File archivoPDF = new File(filePathPDFURL+filePathname);
		    	if(archivoPDF.exists()){
				response.setUrlPdf("./pdf"+filePathname);
		    	}
			
		} catch (Exception e) {
			log.info("error: " + e.getMessage());
			
			return new ResponseEntity<PdfResponse>(response, HttpStatus.BAD_REQUEST) ;
		}
		
		return new ResponseEntity<PdfResponse>(response, HttpStatus.OK) ;
	}
    
    
    @CrossOrigin
	@Transactional
	@RequestMapping(value="generarReporte2", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PdfResponse> generarReporte2(HttpServletRequest req, @RequestBody PdfRequest pdfRequest){	
    	PdfResponse response = new PdfResponse();
		try{
			LocalDateTime ahora = LocalDateTime.now();
			
			String filePathPDFURL ="C:/Users/miSCha/Documents/miSCha/Proyectos_GIT/target/classes/static/pdf";// 
			//String filePathPDFURL = req.getSession().getServletContext().getRealPath("/Proyectos_GIT/target/classes/static/pdf");
			
			String filePathPDF=rutaArchivos;
			String filePathname = "/nombre"+RandomStringUtils.randomAlphanumeric(4)+"_A"+ahora.getYear()+"M"+ahora.getMonthValue()+"D"+ahora.getDayOfMonth()+"H"+ahora.getHour()+"m"+ahora.getHour() +".pdf";
			String mFileFullName=filePathPDF+filePathname;
			Ris rsiOne = risService.getListRisObtenerOne(pdfRequest.getCoRis());
			
			Resource resourceJasper = new ClassPathResource("static/jasper/nominalizacion2_Landscape.jrxml");
			
			
			
			String rutaJasper=resourceJasper.getFile().getAbsolutePath();
	     
			Map<String, Object> parametro = new HashMap<String, Object>();
			
			parametro.put("codRis",String.valueOf(pdfRequest.getCoRis()));
			parametro.put("nomRis",rsiOne.getRis());
			
			
			
			JasperReport report=null;
			JasperPrint jasperPrint=null;
			
			List<String> mListadoPDFsGenerados = new ArrayList<String>();
			mListadoPDFsGenerados.clear();
			
			
				// cargamos el objeto al JASPER
				JRBeanCollectionDataSource ds=new JRBeanCollectionDataSource(pdfRequest.getListaTabla());

				log.info("iniciando compilacion");
				// compilamos el jasper
				report=JasperCompileManager.compileReport(rutaJasper);
				log.info("seteando variables al jasper report");
				
				log.info("rutaJasper: " + rutaJasper);
				log.info("report: " + report.getName());
				
				log.info("parametros: " + parametro.toString());
				
				jasperPrint=JasperFillManager.fillReport(report, parametro, ds);
		
				// Export to PDF.
				log.info("Exportando");
				JasperExportManager.exportReportToPdfFile(jasperPrint, mFileFullName);
				
				byte[] file = null;
				File miPDF = new File(mFileFullName);
			    file = Files.toByteArray(miPDF);
		    	Files.write(file, new File(filePathPDFURL+filePathname));
		    	
		    	File archivoPDF = new File(filePathPDFURL+filePathname);
		    	if(archivoPDF.exists()){
				response.setUrlPdf("./pdf"+filePathname);
		    	}
			
		} catch (Exception e) {
			log.info("error: " + e.getMessage());
			
			return new ResponseEntity<PdfResponse>(response, HttpStatus.BAD_REQUEST) ;
		}
		
		return new ResponseEntity<PdfResponse>(response, HttpStatus.OK) ;
	}
    
}
