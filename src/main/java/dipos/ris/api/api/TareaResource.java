package dipos.ris.api.api;

import java.net.URI;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.Tarea;
import dipos.ris.api.service.TareaService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/tarea")
@RequiredArgsConstructor
public class TareaResource {
    private final TareaService tareaService;

    @GetMapping("/listarTareas")
    @ApiOperation(value = "Este metodo lista todas las tareas")
    public ResponseEntity<List<Tarea>>getTareas() {
        return ResponseEntity.ok().body(tareaService.listarTareas());
    }
   

    @PostMapping("/grabarTarea")
    @ApiOperation(value = "Este metodo Graba una tarea")
    public ResponseEntity<Tarea>grabarCargo(@RequestBody Tarea tar) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/cargo/grabarTarea").toUriString());
        return ResponseEntity.created(uri).body(tareaService.grabarTarea(tar));
    }

    
    
}

