package dipos.ris.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.Sector;

@Repository
public interface SectorRepo extends CrudRepository<Sector, Integer>,
									JpaRepository<Sector, Integer>
{

}
