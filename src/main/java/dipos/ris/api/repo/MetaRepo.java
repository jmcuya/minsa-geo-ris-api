package dipos.ris.api.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.Meta;

@Repository
public interface MetaRepo extends 	CrudRepository<Meta, Integer>,
										PagingAndSortingRepository<Meta, Integer>
										{


	public List<Meta> findByUsuarioIdUsuarioAndAnio(int idUsuario, String anio);
}
