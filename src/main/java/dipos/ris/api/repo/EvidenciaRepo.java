package dipos.ris.api.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.Evidencia;

@Repository
public interface EvidenciaRepo extends CrudRepository<Evidencia, Integer>{
	
	
	public Evidencia findByMetaIdMeta(int idMeta); 

}
