package dipos.ris.api.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import dipos.ris.api.domain.TablaComodinRis;

public interface TablaComodinRisRepository extends CrudRepository<TablaComodinRis, String> {

	@Query(nativeQuery = true, value = "CALL sp_nomDataBuscador(:in_tabla,:in_dni,:in_nombres,:in_paterno,:in_materno,:in_genero,:in_edadIni,:in_edadFin,:in_idZona,:in_idIafas,:in_idSector)")
	public List<TablaComodinRis> getNominalizationBuscador(@Param("in_tabla") String in_tabla,@Param("in_dni") String in_dni,@Param("in_nombres") String in_nombres,@Param("in_paterno") String in_paterno,@Param("in_materno") String in_materno,@Param("in_genero") String in_genero,@Param("in_edadIni") String in_edadIni,@Param("in_edadFin") String in_edadFin,@Param("in_idZona") int in_idZona,@Param("in_idIafas") int in_idIafas,@Param("in_idSector") int in_idSector);
	
	@Query(nativeQuery = true, value = "CALL sp_nomDataExcel(:in_tabla,:in_dni,:in_nombres,:in_paterno,:in_materno,:in_genero,:in_edadIni,:in_edadFin,:in_idZona,:in_idIafas,:in_idSector)")
	public List<TablaComodinRis> getNominalizationExcel(@Param("in_tabla") String in_tabla,@Param("in_dni") String in_dni,@Param("in_nombres") String in_nombres,@Param("in_paterno") String in_paterno,@Param("in_materno") String in_materno,@Param("in_genero") String in_genero,@Param("in_edadIni") String in_edadIni,@Param("in_edadFin") String in_edadFin,@Param("in_idZona") int in_idZona,@Param("in_idIafas") int in_idIafas,@Param("in_idSector") int in_idSector);
}
