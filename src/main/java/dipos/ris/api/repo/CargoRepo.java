package dipos.ris.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.Cargo;

@Repository
public interface CargoRepo extends CrudRepository<Cargo, Integer>,
									JpaRepository<Cargo, Integer>
{

}
