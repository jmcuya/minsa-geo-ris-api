package dipos.ris.api.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.DirecGeneral;

@Repository
public interface DirecGeneralRepo extends CrudRepository<DirecGeneral, Integer>{

}
