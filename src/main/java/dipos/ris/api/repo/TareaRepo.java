package dipos.ris.api.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.Tarea;

@Repository
public interface TareaRepo extends 	CrudRepository<Tarea, Integer>,
									JpaRepository<Tarea, Integer>{
	
	@Query(value = "select o from Tarea o "
			+ " join o.listadoTareaUsuario tu "
			+ " join tu.usuario u "
			+ " where u.estado = 1"
			+ " and u.idUsuario=:idUsuario "
			+ " and (:anio is null or tu.anio =:anio)")
	public List<Tarea> listarTareasUsuario(@Param("idUsuario") int idUsuario, @Param("anio") int anio);

}
