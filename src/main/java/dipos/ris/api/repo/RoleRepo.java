package dipos.ris.api.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.Role;

@Repository
public interface RoleRepo extends CrudRepository<Role, Long> {
    Role findByName(String name);
}
