package dipos.ris.api.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class ExcelRequest {
	private int idUsuario;
	private int anio;

}
