package dipos.ris.api.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class ValidaTareaRequest {
	private String motivo;
	private int idMeta;
	private int estado;

}
