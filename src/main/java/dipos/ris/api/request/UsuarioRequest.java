package dipos.ris.api.request;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class UsuarioRequest implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -825728738541999535L;
	private int idUsuario;
	private int idUniOrganica;
    private int idCargo;
    private int idRole;
    private int idTipoUsuario;
    private int idCondLaboral;
	private int page;
	private int cantItemaPagina;
	private String nombre;
    private String apePaterno;
    private String apeMaterno;
    private String fecNacimiento;
    private String sexo;
    private String usuario;
    private String password;
    private String estado;
    private int idTipoDocumento;
    private String nroDocumento;
    private List<TareaRequest> listadoTareas;
    private int anio;
}
