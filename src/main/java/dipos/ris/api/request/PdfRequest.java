package dipos.ris.api.request;

import java.util.List;

import dipos.ris.api.domain.TablaComodinRis;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class PdfRequest {

	private int coRis;
	private List<TablaComodinRis> listaTabla;

}
