package dipos.ris.api.request;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class EvidenciaRequest implements  Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3269660775075725442L;
	/**
	 * 
	 */
	private int idTipoDocEvidencia;
    private String asunto;
    private String fechaEmision;
    private String fileBase64;    
    
}
