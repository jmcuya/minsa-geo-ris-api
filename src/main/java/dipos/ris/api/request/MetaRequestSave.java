package dipos.ris.api.request;

import java.io.Serializable;

import dipos.ris.api.domain.Tarea;
import dipos.ris.api.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class MetaRequestSave implements  Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -7991873251505546449L;
	
	private int idTarea ;           
    private int idUsuario ;         
    private String anio   ;            
    private String tipo   ;            
    private int valorTrim1Prog;
    private int valorTrim1Ejec  ;  
    private int valorTrim2Prog ;    
    private int valorTrim2ejec ;    
    private int valorTrim3Prog;    
    private int valorTrim3Ejec ;    
    private int valorTrim4Prog ;    
    private int valorTrim4Ejec ;    
    private String estado ;            
    private String motivo  ;           
    private String motivoRechazo;   
    
    private EvidenciaRequest evidenciaRequest;
    
    
}
