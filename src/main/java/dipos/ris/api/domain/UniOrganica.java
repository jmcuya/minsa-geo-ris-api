package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor

@Table(name = "uniorganica")
public class UniOrganica implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4673268065833149561L;
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int idUniOrganica;
    private int idDirecGeneral;
    private String nombre;
    private int estado;
    
    @ManyToOne
	@JoinColumn(name="idDirecGeneral", insertable=false, updatable=false)
    private DirecGeneral direcGeneral;
}
