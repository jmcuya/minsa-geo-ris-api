package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
public class TablaComodinRis implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7916850903947691881L;

	
	@Id
	private String dni;
    private String idmanzana;
    private String cod_reni;
    private String ipress;
    private String id_ris;
    private String ris;
    
    private String nombres_ref;
    private String paterno_ref;
    private String materno_ref;
    private String genero;
    private String edad;
    private String longitud;
    private String latitud;
    private int idzona;
    private int idiafas;
    private int idsector;
    
    private String zona;
    
    private String iafa;
    
    private String sector;

}

