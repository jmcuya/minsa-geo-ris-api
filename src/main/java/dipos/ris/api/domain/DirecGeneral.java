package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "direcgeneral")
public class DirecGeneral implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4814340283674879546L;
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int idDirecGeneral;
    private String nombre;
    private int estado;
    
}
