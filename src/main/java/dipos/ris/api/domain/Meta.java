package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "meta")
public class Meta implements  Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 3446038476502089767L;
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int idMeta;             
    private String anio   ;            
    private String tipo   ;            
    private int valorTrim1Prog;
    private int valorTrim1Ejec  ;  
    private int valorTrim2Prog ;    
    private int valorTrim2Ejec ;    
    private int valorTrim3Prog;    
    private int valorTrim3Ejec ;    
    private int valorTrim4Prog ;    
    private int valorTrim4Ejec ;    
    private String estado ;            
    private String motivo  ;           
    private String motivoRechazo  ;          
    
    
    @ManyToOne
	@JoinColumn(name="idTarea")
    private Tarea tarea;
    @ManyToOne
	@JoinColumn(name="idUsuario")
    private Usuario usuario; 

}
