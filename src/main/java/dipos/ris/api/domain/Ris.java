package dipos.ris.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ris")
public class Ris implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1292770992714171931L;
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private int id_ris;
    private String ris;
}
