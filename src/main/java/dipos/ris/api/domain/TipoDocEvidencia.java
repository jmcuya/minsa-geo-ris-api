package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "tipoDocEvidencia")
public class TipoDocEvidencia implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6246558382999610922L;
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int idTipoDocEvidencia;
    private String nombre;
    private int estado;    

}
