package dipos.ris.api.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class UsuarioDto implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1601011601449906200L;
	private int idUsuario;
    private int idTipoUsuario ;
    private int idTipoDocumento;
    private int idCargo ;
    private int idCondLaboral  ;
    private int idUniOrganica  ;
    private int idRole   ;
    
    private String nroDocumento;
    private String Nombre;
    private String apePaterno;
    private String apeMaterno;
    private String fecNacimiento;
    private String sexo;
    private String usuario;
    private String password;
    private int estado;
}
