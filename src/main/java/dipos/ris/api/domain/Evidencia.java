package dipos.ris.api.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "evidencia")
public class Evidencia implements  Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6645139897749122470L;
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int idEvidencia;
    private String asunto;
    private Date fechaEmision;
    private int estado=1;
    private String fileUrl;    
    
    @ManyToOne
	@JoinColumn(name="idMeta")
    private Meta meta;
    @ManyToOne
	@JoinColumn(name="idTipoDocEvidencia")
    private TipoDocEvidencia tipoDocEvidencia; 

}
