package dipos.ris.api.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "usuario")
@JsonIgnoreProperties({"listadoTareaUsuario"})
public class Usuario implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2230432808811704866L;
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int idUsuario;
    private String nroDocumento;
    @Column(name = "Nombre")
    private String nombre;
    private String apePaterno;
    private String apeMaterno;
    private Date fecNacimiento;
    private String sexo;
    @Column(name = "Usuario")
    private String usuario;
    @Column(name = "Password")
    private String password;
    @Column(name = "Estado")
    private int estado;
    
    @ManyToOne
	@JoinColumn(name="idCargo")
    private Cargo cargo;
    
    @ManyToOne
	@JoinColumn(name="idCondLaboral")
    private CondLaboral condLaboral;
    
    @ManyToOne
	@JoinColumn(name="idUniOrganica")
    private UniOrganica uniOrganica;
    
    @ManyToOne
	@JoinColumn(name="idTipoUsuario")
    private TipoUsuario tipoUsuario;
    
    @ManyToOne
	@JoinColumn(name="idTipoDocumento")
    private TipoDocumento tipoDocumento;
    
    @ManyToOne
	@JoinColumn(name="idRole")
    private Role role;
  
    @JsonIgnore
    @OneToMany(mappedBy="usuario",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<TareaUsuario> listadoTareaUsuario;
  
}
