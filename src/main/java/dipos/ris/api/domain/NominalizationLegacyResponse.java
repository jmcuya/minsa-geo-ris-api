package dipos.ris.api.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class NominalizationLegacyResponse implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7916850903947691881L;

    private String idmanzana;
    private String cod_reni;
    private String ipress;
    private String id_ris;
    private String ris;
    private String dni;
    private String nombres_ref;
    private String paterno_ref;
    private String materno_ref;
    private String genero;
    private String edad;
    private String longitud;
    private String latitud;
    private String zona;
    private String iafa;
    private String sector;

}

