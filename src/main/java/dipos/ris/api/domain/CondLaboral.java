package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "condlaboral")
public class CondLaboral implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1070097100783011945L;
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int idCondLaboral;
    private String nombre;
    private int estado;
}
