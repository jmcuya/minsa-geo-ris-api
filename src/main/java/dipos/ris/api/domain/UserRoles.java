package dipos.ris.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_roles")
public class UserRoles implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3112055484335524689L;
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idUserRoles;
    private int user_id;
    private int roles_id;
}
