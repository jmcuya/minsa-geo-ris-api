package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor

@Table(name = "tipousuario")
public class TipoUsuario implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4974635057368121878L;
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int idTipoUsuario;
    private String TipoUsuario;
    private int estado;
}
