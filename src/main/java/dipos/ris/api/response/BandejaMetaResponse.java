package dipos.ris.api.response;

import java.io.Serializable;

import dipos.ris.api.domain.Tarea;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class BandejaMetaResponse implements  Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -4123517304403536326L;
	/**
	 * 
	 */
	private int idMeta;             
    private Tarea tarea ;           
    private EvidenciaResponse evidenciaResponse; 
    private String estado ;            
    private String motivo  ;           
    private String motivoRechazo;          
    
    
}
