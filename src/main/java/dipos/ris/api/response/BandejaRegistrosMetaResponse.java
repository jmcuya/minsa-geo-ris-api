package dipos.ris.api.response;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class BandejaRegistrosMetaResponse {
	
	List<MetasTrimestresResponse> listaMetasTrimestreResponse;
	List<BandejaMetaResponse> listaBandejaMetaResponse;

}
