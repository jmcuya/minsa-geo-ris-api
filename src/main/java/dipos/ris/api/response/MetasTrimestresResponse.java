package dipos.ris.api.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class MetasTrimestresResponse {
	
	private String trimestre;
	private String ejecutado;
	private String programado;
	private String valor;
	private String porcentaje;

}
