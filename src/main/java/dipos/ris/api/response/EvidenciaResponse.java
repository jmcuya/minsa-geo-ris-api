package dipos.ris.api.response;

import java.io.Serializable;
import java.util.Date;

import dipos.ris.api.domain.TipoDocEvidencia;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class EvidenciaResponse implements  Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6926878491841999567L;
	private int idEvidencia;
    private String asunto;
    private Date fechaEmision;
    private int estado=1;
    private String fileUrl;    
    private TipoDocEvidencia tipoDocEvidencia; 
    private String fileBase64;

}
