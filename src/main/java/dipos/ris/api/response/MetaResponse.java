package dipos.ris.api.response;

import java.io.Serializable;

import dipos.ris.api.domain.Tarea;
import dipos.ris.api.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class MetaResponse implements  Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -4123517304403536326L;
	/**
	 * 
	 */
	private int idMeta;             
    private Tarea tarea ;           
    private Usuario usuario ;         
    private String anio   ;            
    private String tipo   ;            
    private int valorTrim1Prog;
    private int valorTrim1Ejec  ;  
    private int valorTrim2Prog ;    
    private int valorTrim2ejec ;    
    private int valorTrim3Prog;    
    private int valorTrim3Ejec ;    
    private int valorTrim4Prog ;    
    private int valorTrim4Ejec ;    
    private String estado ;            
    private String motivo  ;           
    private String motivoRechazo;          
    
    
}
