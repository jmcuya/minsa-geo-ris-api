package dipos.ris.api.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class PdfResponse {
	
	private String urlPdf;
	private boolean exito;
	private String mensaje;

}
